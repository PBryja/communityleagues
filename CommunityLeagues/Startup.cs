﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CommunityLeagues.Startup))]
namespace CommunityLeagues
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
