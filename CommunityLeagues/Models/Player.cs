﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CommunityLeagues.Models
{
    public class Player
    {
        public int PlayerId { get; set; }

        [MaxLength(30, ErrorMessage = "Max length = 30 chars")]
        [MinLength(4, ErrorMessage = "Min length = 4 chars")]
        [Index("NamePerGame", 1, IsUnique = true)]
        public string Name { get; set; }

        [Required]
        public int Score { get; set; }

        [Index("IX_UserPerGame", 1, IsUnique = true)]
        public string IdentityUserId { get; set; }
        public virtual IdentityUser IdentityUser { get; set; }

        [Index("IX_UserPerGame", 2, IsUnique = true)]
        [Index("NamePerGame", 2, IsUnique = true)]
        public int GameId { get; set; }
        public virtual Game Game { get; set; }
    }
}