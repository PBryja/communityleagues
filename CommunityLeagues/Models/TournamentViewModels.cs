﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommunityLeagues.Models
{
    public class TournamentViewModel
    {
        public int TournamentId { get; set; }
        public string Name { get; set; }
        public int Award { get; set; }
        public DateTime dateStart { get; set; }
        public List<Team> Teams { get; set; }
    }

    public class SignUpViewModel
    {
        public Tournament Tournament { get; set; }
        public Team Team { get; set; }
        public bool EnoughRoleOfPlayer { get; set; }
        public bool TeamIsAlreadySignUp { get; set; }
    }
}