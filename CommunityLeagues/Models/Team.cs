﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CommunityLeagues.Models
{
    public class Team
    {
        public int TeamId { get; set; }

        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(10)]
        public string Tag { get; set; }

        [Required]
        public int Score { get; set; }

        [ForeignKey("Creator")]
        public int CreatorId { get; set; }
        public virtual Player Creator { get; set; }

        public IQueryable<TeamPlayer> TeamPlayers { get; set; }

        public int? TournamentId { get; set; }
        public virtual Tournament Tournament { get; set; }
    }

    public class TeamPlayer
    {
        public int TeamPlayerId { get; set; }

        [Required]
        public Roles TeamRole { get; set; }

        public int? PlayerId { get; set; }
        public virtual Player Player { get; set; }

        public int? TeamId { get; set; }
        public virtual Team Team { get; set; }
    }

    public enum Roles
    {
        Leader = 10,
        ViceLeader = 20,
        Member = 30,
        Trial = 40,
        Substitute = 50
    }
}