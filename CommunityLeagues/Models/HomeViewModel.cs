﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommunityLeagues.Models
{
    public class HomeViewModel
    {
        public List<Game> Games { get; set; }
        public List<Player> Players { get; set; }
        public List<Team> Teams { get; set; }
    }
}