﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CommunityLeagues.Models
{
    public class Game
    {
        public int GameId { get; set; }

        [MaxLength(30)]
        public string Name { get; set; }
    }
}