﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommunityLeagues.Models
{
    public class TeamViewModel
    {
        public Team Team { get; set; }
        public IEnumerable<TeamPlayer> TeamPlayers {get; set;}
        public TeamPlayer UserAsTeamPlayer { get; set; }
        public IEnumerable<Player> FreePlayers { get; set; }
        public Player NewPlayer { get; set; }


    }

    public class CreateTeamViewModel
    {
        public Team Team { get; set; }
        public Player Player { get; set; }
    }
}