﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommunityLeagues.Models
{
    public class Tournament
    {
        public int TournamentId { get; set; }
        public string Name { get; set; }
        public int Award { get; set; }
        public DateTime dateStart { get; set; }

        public virtual ICollection<Team> Teams { get; set; }
    }
}