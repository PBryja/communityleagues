﻿using CommunityLeagues.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CommunityLeagues.Controllers
{
    public class AdminPanelController : Controller
    {
        [Authorize(Roles = UserRoles.ADMIN)]
        public ActionResult Index()
        {
            return View();
        }
    }
}