﻿using CommunityLeagues.DAL;
using CommunityLeagues.Infrastructure;
using CommunityLeagues.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommunityLeagues.Controllers
{
    public class TeamController : Controller
    {
        private LeaguesContext db;
        private TeamManager teamManager;
        private PlayerManager playerManager;

        public TeamController(LeaguesContext db, TeamManager tm, PlayerManager pm)
        {
            this.teamManager = tm;
            this.playerManager = pm;
            this.db = db;
        }

        public ActionResult Index(ManageMessageTeamId? Message)
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Account");

            var userId = User.Identity.GetUserId();

            var team = teamManager.GetTeam(userId);

            if (playerManager.GetPlayers(userId).ToList().Count == 0)
                return RedirectToAction("CreatePlayer", "Manage");


            if (team == null)
                return RedirectToAction("CreateTeam");

            TeamViewModel teamVM = new TeamViewModel();
            teamVM.Team = team;
            teamVM.TeamPlayers = team.TeamPlayers.ToList().OrderBy(a => a.TeamRole);
            teamVM.UserAsTeamPlayer = teamManager.GetTeamPlayer(userId);
            teamVM.FreePlayers = teamManager.GetPlayersWithoutTeam().ToList();

            return View(teamVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(TeamViewModel model)
        {
            var teamPlayer = new TeamPlayer()
            {
                PlayerId = model.NewPlayer.PlayerId,
                TeamId = model.Team.TeamId,
                TeamRole = Roles.Member
            };


            db.TeamPlayers.Add(teamPlayer);

            try
            {
                db.SaveChanges();
            }
            catch (Exception exception)
            {
                return RedirectToAction("Index", new { Message = ManageMessageTeamId.CantAddNewMember });
            }

            return RedirectToAction("Index", new { Message = ManageMessageTeamId.MemberIsAdded });
        }

        public ActionResult Team(int id)
        {
            return View();
        }

        [Authorize]
        public ActionResult CreateTeam()
        {
            var userId = User.Identity.GetUserId();

            if (playerManager.GetPlayers(userId).ToList().Count == 0)
                return RedirectToAction("CreatePlayer", "Manage");

            Player player = playerManager.GetPlayers(userId).ToList()[0];

            CreateTeamViewModel model = new CreateTeamViewModel()
            {
                Player = player
            };

            return View(model);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateTeam(CreateTeamViewModel model)
        {
            var userId = User.Identity.GetUserId();
            Player player = playerManager.GetPlayers(userId).ToList()[0];

            if (ModelState.IsValid)
            {
                TeamPlayer teamPlayer = new TeamPlayer()
                {
                    PlayerId = player.PlayerId,
                    TeamRole = Roles.Leader,
                    TeamId = model.Team.TeamId

                };

                //db.TeamPlayers.Add(model.Player);

                model.Team.Score = 1000;
                model.Team.CreatorId = player.PlayerId;
                db.Team.Add(model.Team);

                try
                {
                    db.SaveChanges();
                    teamPlayer.TeamId = model.Team.TeamId;
                    db.TeamPlayers.Add(teamPlayer);
                    db.SaveChanges();

                }
                catch (DbUpdateException dbException)
                {
                    return RedirectToAction("Index", new { Message = "Błąd" });
                }
                catch (Exception exception)
                {
                    return RedirectToAction("Index", new { Message = ManageMessageTeamId.NameIsNotAvailable });
                }

                return RedirectToAction("Index");
            }

            return View(model);
        }

        public enum ManageMessageTeamId
        {
            NameIsNotAvailable,
            CantAddNewMember,
            MemberIsAdded
        }
    }
}