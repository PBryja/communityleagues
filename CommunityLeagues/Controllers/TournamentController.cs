﻿using CommunityLeagues.DAL;
using CommunityLeagues.Infrastructure;
using CommunityLeagues.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CommunityLeagues.Controllers
{
    public class TournamentController : Controller
    {

        private LeaguesContext db;
        private TournamentManager tournamentManager;
        //private TeamManager teamManager;
        //private PlayerManager playerManager;

        public TournamentController(LeaguesContext db, TournamentManager tm)
        {
            this.db = db;
            tournamentManager = tm;
        }
        // GET: Tournament
        public ActionResult Index(int id)
        {
            Tournament tournament = tournamentManager.GetTournament(id);
            TournamentViewModel tournamentVM = new TournamentViewModel()
            {
                TournamentId = tournament.TournamentId,
                Award = tournament.Award,
                Name = tournament.Name,
                Teams = tournament.Teams.OrderBy(a => a.Name).ToList(),
                dateStart = tournament.dateStart
            };

            return View(tournamentVM);
        }

        [Authorize]
        public ActionResult SignUp(int id)
        {
            var userId = User.Identity.GetUserId();

            var pm = new PlayerManager(db);
            var tm = new TeamManager(db);

            var signupVM = new SignUpViewModel()
            {
                Team = tm.GetTeam(userId),
                Tournament = tournamentManager.GetTournament(id),
                EnoughRoleOfPlayer = true,
                TeamIsAlreadySignUp = false
            };

            if (tm.GetTeamPlayer(userId).TeamRole != Roles.Leader)
            {
                signupVM.EnoughRoleOfPlayer = false;
            }
            else if (tournamentManager.IsTeamSignUp(signupVM.Team.TeamId, id))
            {
                signupVM.TeamIsAlreadySignUp = true;
            }

            return View(signupVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignUp(SignUpViewModel sm)
        {
            var team = sm.Team;
            var result = db.Team.Where(a => a.TeamId == team.TeamId).FirstOrDefault();
            result.TournamentId = sm.Tournament.TournamentId;
            db.SaveChanges();

            return RedirectToAction("Index", new { id = sm.Tournament.TournamentId });
        }
    }
}