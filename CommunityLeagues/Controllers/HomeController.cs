﻿using CommunityLeagues.DAL;
using CommunityLeagues.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CommunityLeagues.Controllers
{
    public class HomeController : Controller
    {
        LeaguesContext db;

        public ActionResult Index(LeaguesContext db)
        {
            HomeViewModel homeVM = new HomeViewModel()
            {
                Games = db.Games.ToList(),
                Teams = db.Team.ToList(),
                Players = db.Players.ToList()
            };

            return View(homeVM);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}