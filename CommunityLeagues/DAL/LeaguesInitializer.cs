﻿using CommunityLeagues.Migrations;
using CommunityLeagues.Models;
using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CommunityLeagues.DAL
{
    public class LeaguesInitializer : DropCreateDatabaseAlways<LeaguesContext>
    {
        public override void InitializeDatabase(LeaguesContext context)
        {
            //context.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction
            //    , string.Format("ALTER DATABASE {0} SET SINGLE_USER WITH ROLLBACK IMMEDIATE", context.Database.Connection.Database));

            base.InitializeDatabase(context);
        }

        protected override void Seed(LeaguesContext context)
        {
            SeedStoreData(context);

            base.Seed(context);
        }

        public static void SeedStoreData(LeaguesContext context)
        {
            var game = new Game { Name = "Counter Strike GO" };
            context.Games.AddOrUpdate(game);
            context.SaveChanges();


            //var games = new List<Game>
            //{
            //    {new Game() { Name = "CS Test"} }
            //};

            //games.ForEach(a => context.Games.AddOrUpdate(a));
            context.SaveChanges();

            var store = new RoleStore<IdentityRole>(context);
            var manager = new RoleManager<IdentityRole>(store);
            var role = new IdentityRole { Name = UserRoles.ADMIN };

            manager.Create(role);

            var store2 = new UserStore<ApplicationUser>(context);
            var manager2 = new UserManager<ApplicationUser>(store2);
            var user = new ApplicationUser { Email = "admin@admin.pl", UserName = "admin@admin.pl", Id = "adminId" };
            var user2 = new ApplicationUser { Email = "bryja.patryk3@gmail.com", UserName = "bryja.patryk3@gmail.com", Id = "mojId" };
            var user3 = new ApplicationUser { Email = "bryja.patryk4@gmail.com", UserName = "bryja.patryk4@gmail.com" };
            var user4 = new ApplicationUser { Email = "bryja.patryk5@gmail.com", UserName = "bryja.patryk5@gmail.com" };
            var user5 = new ApplicationUser { Email = "bryja.patryk6@gmail.com", UserName = "bryja.patryk6@gmail.com" };
            var user6 = new ApplicationUser { Email = "bryja.patryk7@gmail.com", UserName = "bryja.patryk7@gmail.com" };
            var user7 = new ApplicationUser { Email = "bryja.patryk8@gmail.com", UserName = "bryja.patryk8@gmail.com" };
            var user8 = new ApplicationUser { Email = "bryja.patryk9@gmail.com", UserName = "bryja.patryk9@gmail.com" };
            var user9 = new ApplicationUser { Email = "bryja.patryk0@gmail.com", UserName = "bryja.patryk0@gmail.com" };


            var result = manager2.Create(user, "t12345");

            if (result.Succeeded == false) { throw new Exception(result.Errors.First()); }
            manager2.AddToRole(user.Id, "Admin");

            var result2 = manager2.Create(user2, "t12345");
            result2 = manager2.Create(user3, "t12345");
            result2 = manager2.Create(user4, "t12345");
            result2 = manager2.Create(user5, "t12345");
            result2 = manager2.Create(user6, "t12345");
            result2 = manager2.Create(user7, "t12345");
            result2 = manager2.Create(user8, "t12345");
            result2 = manager2.Create(user9, "t12345");

            context.SaveChanges();


            var players = new List<Player>
            {
                new Player { GameId = game.GameId, IdentityUserId = user.Id, Name = "AdminPlayer", Score = 10000, PlayerId = 1},
                new Player { GameId = game.GameId, IdentityUserId = user2.Id, Name = "Terminator", Score = 1000, PlayerId = 2 },
                new Player { GameId = game.GameId, IdentityUserId = user3.Id, Name = "Maniac", Score = 1000, PlayerId = 3 },
                new Player { GameId = game.GameId, IdentityUserId = user4.Id, Name = "Coctailooo", Score = 1000, PlayerId = 4 },
                new Player { GameId = game.GameId, IdentityUserId = user5.Id, Name = "Amateur", Score = 1000, PlayerId = 5 },
                new Player { GameId = game.GameId, IdentityUserId = user6.Id, Name = "Rambo", Score = 1000, PlayerId = 6 },
                new Player { GameId = game.GameId, IdentityUserId = user7.Id, Name = "Angry Bird", Score = 1000, PlayerId = 7 },
                new Player { GameId = game.GameId, IdentityUserId = user8.Id, Name = "Mortal", Score = 1000, PlayerId = 8 },
                new Player { GameId = game.GameId, IdentityUserId = user9.Id, Name = "Pascal", Score = 1000, PlayerId = 9 },

            };

            players.ForEach(g => context.Players.AddOrUpdate(g));
            context.SaveChanges();


            var tournaments = new Tournament { Award = 50000, dateStart = DateTime.Now.AddDays(30), TournamentId = 1, Name = "Great stars" };
            context.Tournaments.AddOrUpdate(tournaments);
            context.SaveChanges();


            var team = new List<Team>
            {
                new Team { CreatorId = 7, Name = "AngryBirds", Score = 9500, Tag = "Bird.", TeamId = 1, TournamentId = 1},
                new Team { CreatorId = 1, Name = "TheBests", Score = 9500, Tag = "Best: ", TeamId = 2},
                new Team { CreatorId = 5, Name = "Virtual Pro", Score = 9500, Tag = "/VPO/", TeamId = 3},

            };

            team.ForEach(g => context.Team.AddOrUpdate(g));
            context.SaveChanges();


            var teamPlayers = new List<TeamPlayer>
            {
                new TeamPlayer { PlayerId = 7, TeamId = 1, TeamRole = Roles.Leader },
                new TeamPlayer { PlayerId = 3, TeamId = 1, TeamRole = Roles.Member },
                new TeamPlayer { PlayerId = 2, TeamId = 1, TeamRole = Roles.Member },
                new TeamPlayer { PlayerId = 1, TeamId = 2, TeamRole = Roles.Leader },
                new TeamPlayer { PlayerId = 4, TeamId = 3, TeamRole = Roles.Leader },
                new TeamPlayer { PlayerId = 5, TeamId = 3, TeamRole = Roles.Member },
            };

            teamPlayers.ForEach(g => context.TeamPlayers.AddOrUpdate(g));

        

            context.SaveChanges();
        }
    }
}