﻿using CommunityLeagues.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace CommunityLeagues.DAL
{
    public class LeaguesContext : IdentityDbContext
    {
        public LeaguesContext() : base("LeaguesContext")
        {

        }

        static LeaguesContext()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<LeaguesContext>());

            Database.SetInitializer<LeaguesContext>(new LeaguesInitializer2());
            Database.SetInitializer<LeaguesContext>(new LeaguesInitializer());
        }

        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<Team> Team { get; set; }
        public virtual DbSet<TeamPlayer> TeamPlayers { get; set; }
        public virtual DbSet<Tournament> Tournaments { get; set; }


        public static LeaguesContext Create()
        {
            return new LeaguesContext();
        }
    }
}
