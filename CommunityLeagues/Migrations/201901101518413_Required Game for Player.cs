namespace CommunityLeagues.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RequiredGameforPlayer : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Players", "Game_GameId", "dbo.Games");
            DropIndex("dbo.Players", new[] { "Game_GameId" });
            AlterColumn("dbo.Players", "Game_GameId", c => c.Int(nullable: false));
            CreateIndex("dbo.Players", "Game_GameId");
            AddForeignKey("dbo.Players", "Game_GameId", "dbo.Games", "GameId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Players", "Game_GameId", "dbo.Games");
            DropIndex("dbo.Players", new[] { "Game_GameId" });
            AlterColumn("dbo.Players", "Game_GameId", c => c.Int());
            CreateIndex("dbo.Players", "Game_GameId");
            AddForeignKey("dbo.Players", "Game_GameId", "dbo.Games", "GameId");
        }
    }
}
