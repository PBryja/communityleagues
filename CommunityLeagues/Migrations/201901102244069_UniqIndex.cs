namespace CommunityLeagues.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UniqIndex : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Players", new[] { "IdentityUserId" });
            DropIndex("dbo.Players", new[] { "GameId" });
            CreateIndex("dbo.Players", new[] { "IdentityUserId", "GameId" }, unique: true, name: "IX_UserPerGame");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Players", "IX_UserPerGame");
            CreateIndex("dbo.Players", "GameId");
            CreateIndex("dbo.Players", "IdentityUserId");
        }
    }
}
