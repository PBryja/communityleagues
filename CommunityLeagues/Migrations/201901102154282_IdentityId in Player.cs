namespace CommunityLeagues.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdentityIdinPlayer : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Players", name: "IdentityUser_Id", newName: "IdentityUserId");
            RenameIndex(table: "dbo.Players", name: "IX_IdentityUser_Id", newName: "IX_IdentityUserId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Players", name: "IX_IdentityUserId", newName: "IX_IdentityUser_Id");
            RenameColumn(table: "dbo.Players", name: "IdentityUserId", newName: "IdentityUser_Id");
        }
    }
}
