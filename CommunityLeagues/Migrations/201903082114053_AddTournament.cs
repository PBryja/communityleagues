namespace CommunityLeagues.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTournament : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tournaments",
                c => new
                    {
                        TournamentId = c.Int(nullable: false, identity: true),
                        Award = c.Int(nullable: false),
                        dateStart = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.TournamentId);
            
            AddColumn("dbo.Teams", "Tournament_TournamentId", c => c.Int());
            CreateIndex("dbo.Teams", "Tournament_TournamentId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Teams", new[] { "Tournament_TournamentId" });
            DropColumn("dbo.Teams", "Tournament_TournamentId");
            DropTable("dbo.Tournaments");
        }
    }
}
