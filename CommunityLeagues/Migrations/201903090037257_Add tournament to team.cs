namespace CommunityLeagues.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addtournamenttoteam : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Teams", new[] { "Tournament_TournamentId" });
            RenameColumn(table: "dbo.Teams", name: "Tournament_TournamentId", newName: "TournamentId");
            AlterColumn("dbo.Teams", "TournamentId", c => c.Int(nullable: false));
            CreateIndex("dbo.Teams", "TournamentId");
            AddForeignKey("dbo.Teams", "TournamentId", "dbo.Tournaments", "TournamentId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Teams", "TournamentId", "dbo.Tournaments");
            DropIndex("dbo.Teams", new[] { "TournamentId" });
            AlterColumn("dbo.Teams", "TournamentId", c => c.Int());
            RenameColumn(table: "dbo.Teams", name: "TournamentId", newName: "Tournament_TournamentId");
            CreateIndex("dbo.Teams", "Tournament_TournamentId");
        }
    }
}
