namespace CommunityLeagues.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Nullabletournaments : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Teams", "TournamentId", "dbo.Tournaments");
            DropIndex("dbo.Teams", new[] { "TournamentId" });
            AlterColumn("dbo.Teams", "TournamentId", c => c.Int());
            CreateIndex("dbo.Teams", "TournamentId");
            AddForeignKey("dbo.Teams", "TournamentId", "dbo.Tournaments", "TournamentId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Teams", "TournamentId", "dbo.Tournaments");
            DropIndex("dbo.Teams", new[] { "TournamentId" });
            AlterColumn("dbo.Teams", "TournamentId", c => c.Int(nullable: false));
            CreateIndex("dbo.Teams", "TournamentId");
            AddForeignKey("dbo.Teams", "TournamentId", "dbo.Tournaments", "TournamentId", cascadeDelete: true);
        }
    }
}
