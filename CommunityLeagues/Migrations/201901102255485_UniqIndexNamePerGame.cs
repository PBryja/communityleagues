namespace CommunityLeagues.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UniqIndexNamePerGame : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Players", new[] { "Name", "GameId" }, unique: true, name: "NamePerGame");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Players", "NamePerGame");
        }
    }
}
