// <auto-generated />
namespace CommunityLeagues.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Nullabletournaments : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Nullabletournaments));
        
        string IMigrationMetadata.Id
        {
            get { return "201903090058284_Nullable tournaments"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
