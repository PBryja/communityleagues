namespace CommunityLeagues.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addnametotournaments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tournaments", "Name", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tournaments", "Name");
        }
    }
}
