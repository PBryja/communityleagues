namespace CommunityLeagues.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Teams : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        TeamId = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                        Tag = c.String(maxLength: 10),
                        Score = c.Int(nullable: false),
                        CreatorId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TeamId)
                .ForeignKey("dbo.Players", t => t.CreatorId, cascadeDelete: true)
                .Index(t => t.CreatorId);
            
            CreateTable(
                "dbo.TeamPlayers",
                c => new
                    {
                        TeamPlayerId = c.Int(nullable: false, identity: true),
                        TeamRole = c.Int(nullable: false),
                        PlayerId = c.Int(),
                        TeamId = c.Int(),
                    })
                .PrimaryKey(t => t.TeamPlayerId)
                .ForeignKey("dbo.Players", t => t.PlayerId)
                .ForeignKey("dbo.Teams", t => t.TeamId)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeamPlayers", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.TeamPlayers", "PlayerId", "dbo.Players");
            DropForeignKey("dbo.Teams", "CreatorId", "dbo.Players");
            DropIndex("dbo.TeamPlayers", new[] { "TeamId" });
            DropIndex("dbo.TeamPlayers", new[] { "PlayerId" });
            DropIndex("dbo.Teams", new[] { "CreatorId" });
            DropTable("dbo.TeamPlayers");
            DropTable("dbo.Teams");
        }
    }
}
