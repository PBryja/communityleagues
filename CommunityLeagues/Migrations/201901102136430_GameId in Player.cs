namespace CommunityLeagues.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GameIdinPlayer : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Players", name: "Game_GameId", newName: "GameId");
            RenameIndex(table: "dbo.Players", name: "IX_Game_GameId", newName: "IX_GameId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Players", name: "IX_GameId", newName: "IX_Game_GameId");
            RenameColumn(table: "dbo.Players", name: "GameId", newName: "Game_GameId");
        }
    }
}
