﻿using CommunityLeagues.DAL;
using CommunityLeagues.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CommunityLeagues.Infrastructure
{
    public class PlayerManager
    {
        private LeaguesContext db;

        public PlayerManager(LeaguesContext db)
        {
            this.db = db;
        }

        public IQueryable<Player> GetPlayers(string userId)
        {
            var players = db.Players.Where(u => u.IdentityUser.Id == userId);

            return players;
        }

        public IQueryable<Game> GetAvailableGames(string userId)
        {
            var gamesOfUser = db.Players.Where(a => a.IdentityUserId == userId).Select(a => a.GameId);
            var games = db.Games.Where(a => !gamesOfUser.Contains(a.GameId)).Select(a => a);

            return games;
        }
    }
}