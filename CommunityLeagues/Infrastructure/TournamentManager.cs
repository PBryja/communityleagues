﻿using CommunityLeagues.DAL;
using CommunityLeagues.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommunityLeagues.Infrastructure
{
    public class TournamentManager
    {
        private LeaguesContext db;

        public TournamentManager(LeaguesContext db)
        {
            this.db = db;
        }

        public bool IsTeamSignUp(int idTeam, int idTournament)
        {
            int teamsId = db.Team.Where(a => a.TeamId == idTeam).Select(a => a.TeamId).FirstOrDefault();
            Tournament tournament = GetTournament(idTournament);

            if (tournament.Teams.Count > 0)
            {
                foreach (var team in tournament.Teams)
                {
                    if (team.TeamId == teamsId)
                        return true;
                }
            }
            return false;
            
        }

        public Tournament GetTournament(int id)
        {
            Tournament tournament = db.Tournaments.Where(a => a.TournamentId == id).FirstOrDefault();

            return tournament;
        }
    }
}