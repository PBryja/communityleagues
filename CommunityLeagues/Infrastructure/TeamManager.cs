﻿using CommunityLeagues.DAL;
using CommunityLeagues.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CommunityLeagues.Infrastructure
{
    public class TeamManager
    {
        private LeaguesContext db;

        public TeamManager(LeaguesContext db)
        {
            this.db = db;
        }

        public IQueryable<TeamPlayer> GetTeamPlayers(int teamId)
        {
            var teamPlayers = db.TeamPlayers.Where(a => a.TeamId == teamId);

            return teamPlayers;
        }

        public IQueryable<Player> GetPlayersWithoutTeam()
        {
            var teamPlayers = db.TeamPlayers.Select(a => a.Player);
            var players = db.Players.Where(a => !teamPlayers.Contains(a));

            return players;
        }

        public TeamPlayer GetTeamPlayer(string userId)
        {
            Player player = db.Players.Where(a => a.IdentityUserId == userId).FirstOrDefault();

            if (player == null)
                return null;

            TeamPlayer teamPlayer = db.TeamPlayers.Where(a => a.PlayerId == player.PlayerId).FirstOrDefault();

            return teamPlayer;
        }

        public Team GetTeam(string userId)
        {
            Player player = db.Players.Where(a => a.IdentityUserId == userId).FirstOrDefault();

            if (player == null)
                return null;

            return GetTeam(player.PlayerId);
        }

        public Team GetTeam(int playerId)
        {
            Team team = db.TeamPlayers.Where(a => a.PlayerId == playerId).Select(a => a.Team).FirstOrDefault();
            if (team != null)
                team.TeamPlayers = GetTeamPlayers(team.TeamId);

            return team;
        }

        public IQueryable<Team> GetTeams(int playerId)
        {
            var teams = db.TeamPlayers.Where(a => a.PlayerId == playerId).Select(a => a.Team).OrderBy(a => a.Name);

            return teams;
        }

        public bool IsAvailableName(string name)
        {
            var names = db.TeamPlayers.Where(a => a.Team.Name.ToLower() == name.ToLower());

            if (names == null)
                return false;

            return true;
        }

        public bool IsAvailableTag(string tag)
        {
            var tags = db.TeamPlayers.Where(a => a.Team.Tag.ToLower() == tag.ToLower());

            if (tags == null)
                return false;

            return true;
        }

        public bool IsAvailable(string name, string tag)
        {
            if (IsAvailableTag(tag) == false)
                return false;

            if (IsAvailableName(name) == false)
                return false;

            return true;
        }
    }
}