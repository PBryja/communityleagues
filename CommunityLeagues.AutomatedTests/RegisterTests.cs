﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace CommunityLeagues.AutomatedTests
{
    [TestFixture]
    public class RegisterTests
    {
        IWebDriver _driver;
        string Url = "http://localhost:60454/";

        [SetUp]
        public void StartBrowser()
        {
            _driver = new FirefoxDriver();
        }

        [Test]
        public void RegisterButton_NotLoggedIn_ButtonExistsAndRedirectToRegisterPage()
        {
            _driver.Navigate().GoToUrl(Url);
            var registerButton = _driver.FindElement(By.Id("registerLink"));
            registerButton.Click();

            var findElement = _driver.FindElement(By.XPath("/ html / body / div[2] / h2"));
            Assert.AreEqual("Register.", findElement.Text);
        }

        [Test]
        public void Register_NotExistingUser_RegisterSuccess()
        {
            var newUrl = Url + @"/Account/Register";
            _driver.Navigate().GoToUrl(newUrl);

            var email = "bryja.patryk.test99@gmail.com";
            var pw = "t12345";
            var confirmPw = "t12345";


            var emailField = _driver.FindElement(By.Id("Email"));
            var passwordField = _driver.FindElement(By.Id("Password"));
            var confirmPassword = _driver.FindElement(By.Id("ConfirmPassword"));

            emailField.SendKeys(email);
            passwordField.SendKeys(pw);
            confirmPassword.SendKeys(confirmPw + Keys.Enter);

            new WebDriverWait(_driver, TimeSpan.FromSeconds(10))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists((By.ClassName(@"hello-user"))));

            var hello = _driver.FindElement(By.ClassName(@"hello-user"));

            Assert.AreEqual("Hello "+email+"!", hello.Text);
        }

        [Test]
        public void Register_ExistingUser_RegisterFailed()
        {
            var newUrl = Url + @"/Account/Register";
            _driver.Navigate().GoToUrl(newUrl);

            var email = "admin@admin.pl";
            var pw = "t12345";
            var confirmPw = "t12345";


            var emailField = _driver.FindElement(By.Id("Email"));
            var passwordField = _driver.FindElement(By.Id("Password"));
            var confirmPassword = _driver.FindElement(By.Id("ConfirmPassword"));

            emailField.SendKeys(email);
            passwordField.SendKeys(pw);
            confirmPassword.SendKeys(confirmPw + Keys.Enter);

            var xpath = @"//div[contains(@class, 'validation-summary-errors text-danger')]/ul/li[1]";

            new WebDriverWait(_driver, TimeSpan.FromSeconds(10))
                .Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementExists((By.XPath(xpath))));

            var warning = _driver.FindElement(By.XPath(xpath));
            var expected = "Name " + email + " is already taken.";

            Assert.AreEqual(expected, warning.Text);
        }

        [TearDown]
        public void CloseBrowser()
        {
            _driver.Close();
        }

    }
}
