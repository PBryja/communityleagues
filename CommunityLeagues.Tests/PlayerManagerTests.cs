﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityLeagues.DAL;
using CommunityLeagues.Infrastructure;
using CommunityLeagues.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Moq;
using NUnit.Framework;

namespace CommunityLeagues.Tests
{
    [TestFixture]
    class PlayerManagerTests
    {
        private Mock<DbSet<Game>> gameMock;
        private Mock<DbSet<IdentityUser>> userMock;
        private Mock<DbSet<Player>> playerMock;
        private Mock<LeaguesContext> dbMock;
        private LeaguesContext dbMockObject;

        [OneTimeSetUp]
        public void SetUp()
        {
            dbMock = new Mock<LeaguesContext>();
            gameMock = new Mock<DbSet<Game>>();
            userMock = new Mock<DbSet<IdentityUser>>();
            playerMock = new Mock<DbSet<Player>>();

            dbMockObject = dbMock.Object;

            var gamesData = new List<Game>
            {
                new Game {Name ="game1", GameId= 1 },
                new Game {Name ="game2", GameId= 2 },

            }.AsQueryable();

            var playersData = new List<Player>
            {
                new Player {IdentityUserId = "user1", GameId = 1},
                new Player {IdentityUserId = "user1", GameId = 2},
                new Player {IdentityUserId = "user2", GameId = 1},
                new Player {IdentityUserId = "user3", GameId = 2},
            }.AsQueryable();

            var usersData = new List<IdentityUser>
            {
                new IdentityUser { Id = "user1" },
                new IdentityUser { Id = "user2" },
                new IdentityUser { Id = "user3" },
                new IdentityUser { Id = "user4" },
            }.AsQueryable();

            SetSetupsForDbSet(gameMock, gamesData);
            SetSetupsForDbSet(playerMock, playersData);
            SetSetupsForDbSet(userMock, usersData);

            dbMock.Setup(db => db.Games).Returns(gameMock.Object);
            dbMock.Setup(db => db.Users).Returns(userMock.Object);
            dbMock.Setup(db => db.Players).Returns(playerMock.Object);

            dbMockObject.SaveChanges();
        }

        public void SetSetupsForDbSet<T>(Mock<DbSet<T>> mock, IQueryable<T> data)
            where T : class
        {
            mock.As<IQueryable<T>>().Setup(m => m.Provider).Returns(data.Provider);
            mock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            mock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
        }

        [TestCase("user1")]
        [TestCase("user2",  "game2")]
        [TestCase("user3", "game1")]
        [TestCase("user4", "game1", "game2")]
        [TestCase("userNotExists", "game1", "game2")]
        [Test]
        public void GetAvailableGames_ProperValues_expected(string userId, params string[] gamesExpected)
        {
            PlayerManager pm = new PlayerManager(dbMockObject);

            string[] availableGames = pm.GetAvailableGames(userId).Select(a => a.Name).ToArray();

            CollectionAssert.AreEqual(gamesExpected, availableGames);
        }
    }
}
