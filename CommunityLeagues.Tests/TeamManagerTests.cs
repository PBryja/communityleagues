﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommunityLeagues.DAL;
using CommunityLeagues.Infrastructure;
using CommunityLeagues.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Moq;
using NUnit.Framework;

namespace CommunityLeagues.Tests
{
    [TestFixture]
    class TeamManagerTests
    {
        private Mock<DbSet<Team>> teamMock;
        private Mock<DbSet<TeamPlayer>> teamPlayerMock;
        private Mock<DbSet<Player>> playerMock;
        private Mock<LeaguesContext> dbMock;
        private LeaguesContext dbMockObject;

        [OneTimeSetUp]
        public void SetUp()
        {
            dbMock = new Mock<LeaguesContext>();
            teamPlayerMock = new Mock<DbSet<TeamPlayer>>();
            teamMock = new Mock<DbSet<Team>>();
            playerMock = new Mock<DbSet<Player>>();

            dbMockObject = dbMock.Object;

            var playersData = new List<Player>
            {
                new Player {IdentityUserId = "user1", GameId = 1, PlayerId = 1},
                new Player {IdentityUserId = "user2", GameId = 2, PlayerId = 2},
                new Player {IdentityUserId = "user3", GameId = 3, PlayerId = 3},
            }.AsQueryable();

            var teamsData = new List<Team>
            {
                new Team {Name = "Team1", Tag = "T1", Score = 10000, TeamId = 100, CreatorId = 1},
                new Team {Name = "Team2", Tag = "T2", Score = 5000, TeamId = 200, CreatorId = 2},
                new Team {Name = "Team3", Tag = "T3", Score = 7000, TeamId = 300, CreatorId = 3}
            }.AsQueryable();

            var teamsPlayersData = new List<TeamPlayer>
            {
                new TeamPlayer {TeamId = 100, PlayerId = 1, TeamRole=Roles.Leader, TeamPlayerId = 10, Team = teamsData.ToList()[0]},
                new TeamPlayer {TeamId = 200, PlayerId = 2, TeamRole=Roles.Leader, TeamPlayerId = 20, Team = teamsData.ToList()[1]},
                new TeamPlayer {TeamId = 300, PlayerId = 3, TeamRole=Roles.Leader, TeamPlayerId = 30, Team = teamsData.ToList()[2]},
                new TeamPlayer {TeamId = 200, PlayerId = 3, TeamRole=Roles.Member, TeamPlayerId = 40, Team = teamsData.ToList()[1]},
            }.AsQueryable();



            var usersData = new List<IdentityUser>
            {
                new IdentityUser { Id = "user1" },
                new IdentityUser { Id = "user2" },
                new IdentityUser { Id = "user3" },
                new IdentityUser { Id = "user4" },
            }.AsQueryable();

            SetSetupsForDbSet(teamMock, teamsData);
            SetSetupsForDbSet(playerMock, playersData);
            SetSetupsForDbSet(teamPlayerMock, teamsPlayersData);

            dbMock.Setup(db => db.TeamPlayers).Returns(teamPlayerMock.Object);
            dbMock.Setup(db => db.Team).Returns(teamMock.Object);
            dbMock.Setup(db => db.Players).Returns(playerMock.Object);

            dbMockObject.SaveChanges();
        }

        public void SetSetupsForDbSet<T>(Mock<DbSet<T>> mock, IQueryable<T> data)
            where T : class
        {
            mock.As<IQueryable<T>>().Setup(m => m.Provider).Returns(data.Provider);
            mock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(data.Expression);
            mock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
        }

        [TestCase(1, "Team1")]
        [TestCase(2, "Team2")]
        [Test]
        public void GetTeam_ExistingPlayers_expectedTeam(int playerId, string teamExpected)
        {
            TeamManager teamManager = new TeamManager(dbMockObject);

            string name = teamManager.GetTeam(playerId).Name;

            Assert.AreEqual(teamExpected, name);
        }

        [TestCase(0)]
        [Test]
        public void GetTeam_NotExistingPlayer_NullExpected(int playerId)
        {
            TeamManager teamManager = new TeamManager(dbMockObject);

            Team team = teamManager.GetTeam(playerId);

            Assert.AreEqual(null, team);
        }
    }
}
